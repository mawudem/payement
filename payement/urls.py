"""payement URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib.auth import views as auth_views
from django.contrib import admin
from django.urls import path
from paygate import views as pv
from accounts import views as av


urlpatterns = [
    path('admin/', admin.site.urls),
    path('accounts/login/',av.login ),
    path('register/', av.register),
    #path('accounts/login/', auth_views.LoginView.as_view(template_name='Accueil.html')),
    path('accounts/register/', auth_views.LoginView.as_view(template_name='register.html')),
    path('paygate/', pv.home),
    path('paid/', pv.pay),
    path('paid/success/', pv.success),
    path('', av.index),
    #path('change-password/',auth_views.PasswordChangeView.as_view(template_name='change-password.html')),

    
]
