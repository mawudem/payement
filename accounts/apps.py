from django.apps import AppConfig


class AcsountsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'acsounts'
